# Keepass
- [Simple guide to creating encrypted, thin provisioned zvol's easily mounted from windows or linux desktop - XigmaNAS](https://www.xigmanas.com/forums/viewtopic.php?t=8664)
- [mirror](http://web.archive.org/*/https://www.xigmanas.com/forums/viewtopic.php?t=8664) [|](http://web.archive.org/web/20190812145928/https://www.xigmanas.com/forums/viewtopic.php?t=8664)
- tag: Zvol, Keepass

# Architecture
- [ZFS Encryption at Rest in OpenZFS - Philipp's Tech Blog](https://blog.heckel.io/2017/01/08/zfs-encryption-openzfs-zfs-on-linux/)

# Guide
- https://datacenteroverlords.com/2017/12/17/zfs-and-linux-and-encryption-part-1/
- https://datacenteroverlords.com/2017/12/17/zfs-on-linux-with-encryption-part-2/

Proxmox
- https://pve.proxmox.com/wiki/ZFS_on_Linux#zfs_encryption

NixOS
- https://nixos.wiki/wiki/NixOS_on_ZFS#Encrypted_ZFS

# thin zvol
create encrypted thin zvol:

```zfs create -s -V 10gb -o encryption=aes-256-gcm -o keyformat=passphrase uni/test```

# load-key
Use `zfs load-key uni/{{zvol}}` to unlock an Encrypted Zvol! It then shows up at /dev/zvol/{{zpool}}/

```
ls /dev/zvol/uni
zfs load-key uni/test
ls /dev/zvol/uni
```


# SIMD on Kernel 5.0
- https://news.ycombinator.com/item?id=20186458

Quote: "	
donmcronald 58 days ago [-]
I remember reading about BTRFS parity patches that were shelved because they didn't align with someone's business interests. Maybe a storage vendor is exerting some pressure to impede modern file systems on Linux."

Only performant on NixOS currently
- https://www.phoronix.com/scan.php?page=news_item&px=NixOS-Linux-5.0-ZFS-FPU-Drop
